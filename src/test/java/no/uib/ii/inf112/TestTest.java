package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			text.strip();
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			int extra = (width - text.length());
			text.strip();
			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			int extra = (width - text.length());
			text.strip();
			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};


	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" boo ", aligner.center("boo", 5));
	}

	@Test
	void testFlushRight () {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
		assertEquals("  boo", aligner.flushRight("boo", 5));
	}

	@Test
	void testFlushLeft () {
		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
		assertEquals("boo  ", aligner.flushLeft("boo", 5));
	}
}
